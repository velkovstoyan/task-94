import EventEmitter from "eventemitter3";

export default class Application extends EventEmitter {
  static get events() {
    return {
      READY: "ready",
    };
  }

  constructor() {
    super();
    this.emojis = [];
    this.banana = "🍌";
    this.emit(Application.events.READY);
  }
  setEmojis(emojis) {
    this.emojis = emojis;
  }

  addBananas() {}

  const wrapperDiv = document.getElementById("emojis");

  function addBananas(arr) {
    return arr.map(element => element + " 🍌");
  }

  function setEmojis(arr) {
  // Clear the content of the wrapper div
    wrapperDiv.innerHTML = "";

  // Add a paragraph element for each element in the array
    arr.forEach(element => {
    const p = document.createElement("p");
    p.textContent = element;
    wrapperDiv.appendChild(p);
  });
  }

  const monkeys = emojis;
  const monkeysWithBananas = addBananas(monkeys);
  setEmojis(monkeysWithBananas);
}
